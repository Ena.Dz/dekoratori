package com.dekoratori.demo;

public class Milk extends CondimentDecorator{
    Milk(Drinks basedrink) {
        super(basedrink);
    }

    @Override
    public String getDescription() {
        return basedrink.getDescription() +", " + "Milk";
    }

    @Override
    public double getPrice() {
        return basedrink.getPrice()+0.30;
    }

}
