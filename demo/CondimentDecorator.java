package com.dekoratori.demo;

abstract class CondimentDecorator implements Drinks{

    Drinks basedrink;

    CondimentDecorator(Drinks basedrink){
        this.basedrink=basedrink;
    }

    @Override
    public String getDescription() {
        return basedrink.getDescription();
    }

    @Override
    public double getPrice() {
        return basedrink.getPrice();
    }

}