package com.dekoratori.demo;

public class Amerikano implements Drinks {
    @Override
    public String getDescription() {
        return "Americano";
    }

    @Override
    public double getPrice() {
        return 2.00;
    }
}
