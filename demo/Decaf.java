package com.dekoratori.demo;

public class Decaf implements Drinks {
    @Override
    public String getDescription() {
        return "Decaf Coffee";
    }

    @Override
    public double getPrice() {
        return 2.50;
    }
}
