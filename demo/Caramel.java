package com.dekoratori.demo;

public class Caramel extends CondimentDecorator{
    Caramel(Drinks basedrink) {
        super(basedrink);
    }

    @Override
    public String getDescription() {
        return basedrink.getDescription() +", " + "Caramel";
    }

    @Override
    public double getPrice() {
        return basedrink.getPrice()+0.50;
    }

}