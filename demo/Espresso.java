package com.dekoratori.demo;

public class Espresso implements Drinks {
    @Override
    public String getDescription() {
        return "Espresso";
    }

    @Override
    public double getPrice() {
        return 1.50;
    }
}
