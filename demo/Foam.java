package com.dekoratori.demo;

public class Foam extends CondimentDecorator{
    Foam(Drinks basedrink) {
        super(basedrink);
    }

    @Override
    public String getDescription() {
        return basedrink.getDescription() +", " + "Foam";
    }

    @Override
    public double getPrice() {
        return basedrink.getPrice()+0.10;
    }

}