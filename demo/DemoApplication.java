package com.dekoratori.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {

        Drinks drinks=new Milk(new Foam(new Caramel(new Decaf())));

        System.out.println("Ingredients: " + drinks.getDescription() + "\n" + "Price: " + drinks.getPrice());

    }

}
