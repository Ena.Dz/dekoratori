package com.dekoratori.demo;

public class Cinnamon extends CondimentDecorator{
    Cinnamon(Drinks basedrink) {
        super(basedrink);
    }

    @Override
    public String getDescription() {
        return basedrink.getDescription() +", " + "Cinnamon";
    }

    @Override
    public double getPrice() {
        return basedrink.getPrice()+0.30;
    }

}